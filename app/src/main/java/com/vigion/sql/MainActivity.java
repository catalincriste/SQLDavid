package com.vigion.sql;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.sql.Connection;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private Button buttonLigaSql;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonLigaSql = (Button)findViewById(R.id.button1);
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);

        //ConnectionSQL connectionSQL = new ConnectionSQL();
        //Connection conn = connectionSQL.connection();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonMYsql:
                        Toast.makeText(getBaseContext(), "MySQL", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButtonSQLserver:
                        Toast.makeText(getBaseContext(), "SQL server", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButtonSQLlite:
                        Toast.makeText(getBaseContext(), "SQL lite", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getBaseContext(), "ERRO", Toast.LENGTH_SHORT).show();
                        Log.e("Erro","Nada selecionado");
                        break;
                }
            }
        });

        buttonLigaSql.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedID = radioGroup.getCheckedRadioButtonId();
                Intent intent = new Intent(getApplicationContext(),Activity_Login.class);
                switch (selectedID) {
                    case R.id.radioButtonMYsql:
                        Toast.makeText(getBaseContext(), "MySQL", Toast.LENGTH_SHORT).show();
                        intent.putExtra("sgbd", "MySql");
                        startActivity(intent);
                        break;
                    case R.id.radioButtonSQLserver:
                        Toast.makeText(getBaseContext(), "SQL server", Toast.LENGTH_SHORT).show();
                        intent.putExtra("sgbd", "SQLServer");
                        startActivity(intent);
                        break;
                    case R.id.radioButtonSQLlite:
                        Toast.makeText(getBaseContext(), "SQL lite", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getBaseContext(), "ERRO", Toast.LENGTH_SHORT).show();
                        Log.e("Erro", "Nada selecionado");
                        break;
                }
            }
        });

        (findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Activity_Login.class));
            }
        });

    }




}
