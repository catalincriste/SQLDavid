package com.vigion.sql;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Activity_Login extends AppCompatActivity {

    EditText txtUserId, txtUserPass;
    TextView txtView;
    Button btnLogin;
    ProgressBar pbbar;

    String sgbd = "";
    Intent intentObject = null;
    ConnectionSQL connectionSQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity__login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        connectionSQL = new ConnectionSQL();



        txtUserId = (EditText)(findViewById(R.id.editTextUserID));
        txtUserPass = (EditText)(findViewById(R.id.editTextUserPass));
        txtView = (TextView)(findViewById(R.id.textView));
        pbbar = (ProgressBar)(findViewById(R.id.pbbar));
        btnLogin = (Button)(findViewById(R.id.buttonLogin));

        pbbar.setVisibility(View.INVISIBLE);

        intentObject = getIntent();
        sgbd = intentObject.getStringExtra("sgbd");
        txtView.setText(txtView.getText() + " " + "sgbd");

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login login = new Login();
                login.execute();
            }
        });



    }

    public class Login extends AsyncTask<String, String, String>{

        String msg="";
        Boolean sucesso = false;
        String userID = txtUserId.getText().toString();
        String password = txtUserPass.getText().toString();

        @Override
        protected String doInBackground(String... params) {

            if(userID.trim().equals("") || password.trim().equals("")){
                msg = "Introduza as credenciais";
            }
            else
            {
                try {
                    Connection conn = null;
                    if(sgbd.equals("SQLServer")){
                        conn = connectionSQL.connection();
                    }
                    else if(sgbd.equals("MySql")){
                        conn = connectionSQL.mySQLconnection();

                    }
                    if(conn == null)
                    {
                        msg = "Erro na ligação SQL server";
                    }else {
                        String query = "select * from ALuno where Username='" + userID + "' and Password='" + password + "'";
                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery(query);
                        if(rs.next()){
                            msg = "Login bem sucedido";
                            sucesso = true;
                        }
                        else {
                            msg = "Credenciais invalidas";
                            sucesso = false;
                        }
                    }
                }
                catch (Exception ex){
                    sucesso = false;
                    msg = "Exeção: " + ex.getMessage();
                }
            }

            return msg;
        }

        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);//faz aparecer a progress bar
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            pbbar.setVisibility(View.INVISIBLE);
            Toast.makeText(Activity_Login.this, s, Toast.LENGTH_SHORT).show();
            if(sucesso)
            {
                finish();
            }
        }
    }

}
