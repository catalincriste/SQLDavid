package com.vigion.sql;

import android.app.Activity;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Created by a46505 on 06-01-2016.
 */

public class ConnectionSQL {
    private String server = "10.1.16.183";
    private String driverSQLServer = "net.sourceforge.jtds.jdbc.Driver";
    private String database = "Escola";
    private String dbUserId = "tgpsi";
    private String dbPassword = "esferreira123";

    private String mySQLserver = "10.1.16.183";
    private String mySQLdriverMySQL = "com.mysql.jdbc.Driver";
    private String mySQLdatabase = "Escola";
    private String mySQLdbUserId = "root";
    private String mySQLdbPassword = "";

    public Connection connection()
    {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String connURL = null;
        try{
            Class.forName(driverSQLServer);
            connURL = "jdbc:jtds:sqlserver://" + server
                    + ";databaseName=" + database
                    + ";user=" + dbUserId
                    + ";password=" + dbPassword + ";";
            conn = DriverManager.getConnection(connURL);
        }
        catch (SQLException ex){
            Log.e("ERRO sql execption",ex.getMessage() + ex.getErrorCode());
        } catch (ClassNotFoundException ex) {
            Log.e("ERRO class not found", ex.getMessage() + ex.getException());
        } catch (Exception ex) {
            Log.e("ERRO geral exception", ex.getMessage() + ex.getCause());
        }
        Log.i("sql conn","sucesso!");
        return conn;
    }
    //

    public Connection mySQLconnection()
    {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String connURL = null;
        try{
            Class.forName(mySQLdriverMySQL);
            connURL = "jdbc:mysql://" + mySQLserver
                    + ";databaseName=" + mySQLdatabase
                    + ";user=" + mySQLdbUserId
                    + ";password=" + mySQLdbPassword + ";";
            conn = DriverManager.getConnection(connURL);
        }
        catch (SQLException ex){
            Log.e("ERRO sql execption" + " " +connURL,ex.getMessage() + ex.getErrorCode());
        } catch (ClassNotFoundException ex) {
            Log.e("ERRO class not found", ex.getMessage() + ex.getException());
        } catch (Exception ex) {
            Log.e("ERRO geral exception", ex.getMessage() + ex.getCause());
        }
        Log.i("sql conn","sucesso!");
        return conn;
    }

}
